<?php
include_once("simplehtmldom/simple_html_dom.php");
include_once('phpQuery/phpQuery.php');

$mysqli = new mysqli("localhost", "root", "", "banki");
$timestamp = mktime();
/* connection check */
if ($mysqli->connect_errno) {
  printf("Error: %s\n", $mysqli->connect_error);
  exit();
}

$query = "SELECT timestamp FROM kurs  ORDER BY timestamp DESC LIMIT 1;";
$result = $mysqli->query($query);
$row = $result->fetch_array();
$lasttime = $row[0];

$LastModified_unix = $lasttime; // time of the last page change
$LastModified = gmdate("D, d M Y H:i:s \G\M\T", $LastModified_unix);
$IfModifiedSince = false;
if (isset($_ENV['HTTP_IF_MODIFIED_SINCE']))
    $IfModifiedSince = strtotime(substr($_ENV['HTTP_IF_MODIFIED_SINCE'], 5));  
if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']))
    $IfModifiedSince = strtotime(substr($_SERVER['HTTP_IF_MODIFIED_SINCE'], 5));
if ($IfModifiedSince && $IfModifiedSince >= $LastModified_unix) {
    header($_SERVER['SERVER_PROTOCOL'] . ' 304 Not Modified');
    exit;
}
header('Last-Modified: '. $LastModified);

$query = "SELECT company, code, sell, buy FROM kurs  WHERE code in ('usd') AND timestamp ='" . $lasttime . "'  ORDER BY  sell;";
$result = $mysqli->query($query);
?>
 
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Exchange rates</title>
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/main.css">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<script src="https://cdn.anychart.com/js/latest/anychart-bundle.min.js"></script>
</head>
<body>



<div class="container">
    <h1>The current exchange rate on <?= date("Y-m-d H:i:s", $lasttime)?></h1>
    <div class="row">
        <div class="col-md-6">
            <h2>Dollar</h2>
            <table>
                <tr>
                    <th>Bank name</th>
                    <th>Currency</th>
                    <th>Buy</th>
                    <th>Sell</th>
                </tr>


              <?php
              $sell = 0;
              $sell_company = "";
              $buy = 9999;
              $buy_company = "";
              while ($row = $result->fetch_array()) {
                $row[2] = floatval($row[2]);//sell
                $row[3] = floatval($row[3]);//buy

                if ($buy > $row[2]) {
                  $buy = $row[2];
                  $buy_company = bank_caption($row[0]);
                }
                //Finding the minimum price to sell a page change
                if ($row[3] > $sell) {
                  $sell = $row[3];
                  $sell_company = bank_caption($row[0]);
                }
                ?>
                  <tr>
                      <td><?= bank_caption($row[0]) ?></td>
                      <td><?= $row[1] ?></td>
                      <td><?= $row[3] ?></td>
                      <td><?= $row[2] ?></td>
                  </tr>

              <? } ?>
            </table>
            <div class="where">Profitable <b>TO BUY</b> dollars in
                <b><?= $buy_company ?> per <?= $buy ?></b></div>
            <div class="where">Profitable <b>TO SELL</b> dollars in
                <b><?= $sell_company ?> per <?= $sell ?></b></div>
        </div>
        <div class="col-md-6">
            <h2>Euro</h2>

          <?php

          $query = "SELECT company, code, sell, buy FROM kurs  WHERE code in ('eur') AND timestamp ='" . $lasttime . "'  ORDER BY  sell;";
          $result = $mysqli->query($query);
          ?>
            <table>
                <tr>
                    <th>Bank name</th>
                    <th>Currency</th>
                    <th>Buy</th>
                    <th>Sell</th>
                </tr>


              <?php
              $sell = 0;
              $sell_company = "";
              $buy = 9999;
              $buy_company = "";
              while ($row = $result->fetch_array()) {
                $row[2] = floatval($row[2]);//sell
                $row[3] = floatval($row[3]);//buy

                if ($buy > $row[2]) {
                  $buy = $row[2];
                  $buy_company = bank_caption($row[0]);
                }
                //Search for the minimum sale price
                if ($row[3] > $sell) {
                  $sell = $row[3];
                  $sell_company = bank_caption($row[0]);
                }
                ?>
                  <tr>
                      <td><?= bank_caption($row[0]) ?></td>
                      <td><?= $row[1] ?></td>
                      <td><?= $row[3] ?></td>
                      <td><?= $row[2] ?></td>
                  </tr>

              <? } ?>
            </table>
            <div class="where">Profitable <b>TO BUY</b> euro in
                <b><?= $buy_company ?> per <?= $buy ?></b></div>
            <div class="where">Profitable <b>TO SELL</b> euro in
                <b><?= $sell_company ?> per <?= $sell ?></b></div>
        </div>
    </div>
</div>

<?php

$query = "SELECT DISTINCT(timestamp) FROM kurs WHERE code='usd' AND timestamp BETWEEN " . mktime(9,0,0,date("m"),date("d"),date("Y")) . " AND " . mktime(23,59,59,date("m"),date("d"),date("Y")) . " ORDER BY timestamp ASC;";
$result = $mysqli->query($query);
$mas=array();
$c=0;
while ($row = $result->fetch_array()) {

    $ts = $row['timestamp'];


    $query2 = "SELECT company, sell, buy FROM kurs WHERE code='usd' AND timestamp='" . $ts . "' ORDER BY company;";
    $result2 = $mysqli->query($query2);
    $banki = [];

    while ($row2 = $result2->fetch_array()) {
      //

      $banki[$row2['company']] = $row2['sell'];

    }
    $str = [];
    foreach ($banki as $k => $v) {
      $str[] = "bank" . $k . ": " . ($v) . "";
    }
    $str = implode(", ", $str);

    $mas[] = '{x: "' . date("H:i", $ts) . '", ' . $str . '}';
  
  $c++;
}
 

?>

<div id="dollar" style="height: 700px;"></div>
<script>
  // AnyChart code here
  anychart.onDocumentLoad(function() {
    // create chart and set data
    // as Array of Objects
    var chart = anychart.line();
    chart.data({header: ["#", "Pervomayskiy", "Alfa Bank", "Binbank", "Kuban Kredit", "Rosselhozbank", "VTB"],
      rows:[
        <?php
        print implode(",\n", $mas);
        ?>
      ]});
    chart.title("USA today");
    chart.legend(true);
    chart.container("dollar").draw();


    // create a chart


  });
</script>


 

</body>
</html>
<?php
function bank_caption($code) {
  switch ($code) {
    case "alfa":
      return "Alfa Bank";
      break;
    case "binbank":
      return "Binbank";
      break;
    case "rshb":
      return "Rosselhozbank";
      break;
    case "1mbank":
      return "Pervomayskiy";
      break;
    case "kubankredit":
      return "Kuban Kredit";
      break;
    case "vtb":
      return "VTB";
      break;

  }
}

?>
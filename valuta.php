<?php
include_once("simplehtmldom/simple_html_dom.php");
include_once('phpQuery/phpQuery.php');


$mysqli = new mysqli("localhost", "root", "", "trade");
$timestamp = mktime();
/* проверка соединения */
if ($mysqli->connect_errno) {
  printf("Не удалось подключиться: %s\n", $mysqli->connect_error);
  exit();
}

libxml_use_internal_errors(TRUE);


parser_binbank();
parser_vtb();
parser_alfa();
parser_kubankredit();
parser_1mbank();
parser_rshb();

/*
 * Bank Binbank*/
function parser_binbank() {
  global $mysqli, $timestamp;

  $url = "https://www.binbank.ru/private-clients/currency-exchange/profitable-rate/";

  $html = file_get_html($url);

  $CONTENT = "";
  foreach ($html->find('div.bb_course_box table') as $k => $element) {
    $CONTENT .= $element->outertext;
  }
  $html = str_get_html($CONTENT);
  foreach ($html->find('tr') as $k => $element) {
    $value = str_get_html(trim($element->innertext));

    $code = NULL;
    $buy = NULL;
    $sell = NULL;

    $mas = [];
    $flag = FALSE;
    foreach ($value->find('td') as $key => $td) {

      $mas[] = trim(strip_tags($td->innertext));
      $flag = TRUE;
    }

    if ($flag && isset($mas[1])) {
      $code = strtolower($mas[1]);
      $buy = strtolower($mas[2]);
      $sell = strtolower($mas[3]);

      $query = "INSERT INTO kurs (code, buy, sell, timestamp, company) VALUES ('" . $code . "', '" . $buy . "', '" . $sell . "', '" . $timestamp . "', 'binbank')";
      $result = $mysqli->query($query);
    }
  }
}
/*
 * Bank VTB
 * */
function parser_vtb() {
  global $mysqli, $timestamp;

  $url = "https://www.bm.ru/ru/iframe/kursy_valut-podval/";


  $html = getcontent($url);
  $html = str_get_html($html);

  $CONT = [];
  foreach ($html->find('div.footer__rate_currency') as $k => $element) {
    $CONT[] = $element->outertext;
  }

  foreach ($CONT as $key => $CONTENT) {
    $html = str_get_html($CONTENT);
    $code = NULL;
    $buy = NULL;
    $sell = NULL;

    foreach ($html->find('div.footer__rate__name') as $k => $element) {
      $code =  strtolower( $element->innertext );
    }
    foreach ($html->find('div.footer__rate__index') as $k => $element) {
      if($k==0) $buy = floatval( str_replace(",",".", $element->innertext) );
      else $sell = floatval( str_replace(",",".", $element->innertext) );
    }

     $query = "INSERT INTO kurs (code, buy, sell, timestamp, company) VALUES ('" . $code . "', '" . $buy . "', '" . $sell . "', '" . $timestamp . "', 'vtb')";
    $result = $mysqli->query($query);
  }


}

/*
 * Alfa Bank
 * */
function parser_alfa() {
  global $mysqli, $timestamp;

  $url = "https://alfabank.ru/ext-json/0.2/exchange/cash?offset=0&mode=rest";

  $json = file_get_contents($url);

  $mas = json_decode($json, TRUE);
  $cost = [];
  foreach ($mas as $k => $v) {
    $code = NULL;
    $buy = NULL;
    $sell = NULL;

    $max_b = 0;
    $max_s = 0;

    foreach ($v as $key => $value) {
      $time = strtotime($value['date']);

      if ($value['type'] == "sell") {
        $max_s = ($max_s < $time) ? $max_s : $time;
        if (($max_s < $time)) {
          $cost[$k]['sell'] = floatval($value['value']);
        }
      }
      if ($value['type'] == "buy") {
        $max_b = ($max_b < $time) ? $max_b : $time;
        if (($max_b < $time)) {
          $cost[$k]['buy'] = floatval($value['value']);
        }
      }
    }
  }

  foreach ($cost as $code => $v) {
    $buy = $v['buy'];
    $sell = $v['sell'];
    $query = "INSERT INTO kurs (code, buy, sell, timestamp, company) VALUES ('" . $code . "', '" . $buy . "', '" . $sell . "', '" . $timestamp . "', 'alfa')";
    $result = $mysqli->query($query);
  }
}

/*
 * Kuban Kredit
 * */
function parser_kubankredit() {
  global $mysqli, $timestamp;

  $url = "https://www.kubankredit.ru/";

  $html = getcontent($url);

  preg_match('|var rates \= (.*)var selct \= \$\(\'#officeAdresses\'\)\;|Uis', $html, $matches);
  $js = $matches[1];
  $js = rtrim(trim($js),';');
  $mas = json_decode($js, true);

  foreach ($mas[5]['rates'] as $k => $v) {
      $code = strtolower($v['currency']);
      $buy = doubleval($v['purchase']);
      $sell = doubleval($v['sale']);

      $query = "INSERT INTO kurs (code, buy, sell, timestamp, company) VALUES ('" . $code . "', '" . $buy . "', '" . $sell . "', '" . $timestamp . "', 'kubankredit')";
      $result = $mysqli->query($query);
  }
}

/*
 * Банк Первомайский
 * */
function parser_1mbank() {
  global $mysqli, $timestamp;

  $url = "https://www.1mbank.ru/";

  $html = file_get_html($url);

  $CONTENT = "";
  foreach ($html->find('table.default-client-rates-table') as $k => $element) {
    $CONTENT .= $element->outertext;
  }

  $html = str_get_html($CONTENT);
  foreach ($html->find('tr') as $k => $element) {
    //$CONTENT .= $element->outertext;
    $value = str_get_html(trim($element->innertext));

    $code = NULL;
    $buy = NULL;
    $sell = NULL;

    $mas = [];
    $flag = FALSE;
    foreach ($value->find('td') as $key => $td) {
      $mas[] = trim($td->innertext);
      $flag = TRUE;
    }

    if ($flag) {
      $code = strtolower($mas[0]);
      $buy = strtolower($mas[1]);
      $sell = strtolower($mas[2]);


      $query = "INSERT INTO kurs (code, buy, sell, timestamp, company) VALUES ('" . $code . "', '" . $buy . "', '" . $sell . "', '" . $timestamp . "', '1mbank')";
      $result = $mysqli->query($query);
    }
  }
}

/*
 * Росссельхоз банк парсер
 * */
function parser_rshb() {
  global $mysqli, $timestamp;

  $url = "https://www.rshb.ru/branches/krasnodar/";


  $html = file_get_html($url);
  //print $html = file_get_html($url);


  $CONTENT = "";
  foreach ($html->find('table.b-quotes-table') as $k => $element) {
    $CONTENT .= $element->outertext;
    //print ($k ." = " . $element->innertext);

  }


  $html = str_get_html($CONTENT);
  foreach ($html->find('tr') as $k => $element) {
    //$CONTENT .= $element->outertext;
    $value = str_get_html(trim($element->innertext));

    $code = NULL;
    $buy = NULL;
    $sell = NULL;

    $mas = [];
    $flag = FALSE;
    foreach ($value->find('td') as $key => $td) {
      $mas[] = trim($td->innertext);
      $flag = TRUE;
    }

    if ($flag) {
      $code = strtolower($mas[0]);
      $buy = strtolower($mas[1]);
      $sell = strtolower($mas[2]);


      $query = "INSERT INTO kurs (code, buy, sell, timestamp, company) VALUES ('" . $code . "', '" . $buy . "', '" . $sell . "', '" . $timestamp . "', 'rshb')";
      $result = $mysqli->query($query);
    }
  }
}

function getcontent($url) {
  $ch = curl_init();

  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.0.3) Gecko/2008092417 Firefox/3.0.3');
  curl_setopt($ch, CURLOPT_REFERER, $url);
  curl_setopt($ch, CURLOPT_URL, $url); // set url to post to
  curl_setopt($ch, CURLOPT_FAILONERROR, 1);
  curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
  curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
  curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // return into a variable
  curl_setopt($ch, CURLOPT_TIMEOUT, 30); // times out after 4s
  $result = curl_exec($ch); // run the whole process
  curl_close($ch);
  return $result;
}

die();


?>